<?php

namespace App\Filament\Contributor\Resources\BlogResource\Pages;

use App\Filament\Contributor\Resources\BlogResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateBlog extends CreateRecord
{
    protected static string $resource = BlogResource::class;
}
