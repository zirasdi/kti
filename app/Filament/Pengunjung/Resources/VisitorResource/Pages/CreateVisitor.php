<?php

namespace App\Filament\Pengunjung\Resources\VisitorResource\Pages;

use App\Filament\Pengunjung\Resources\VisitorResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateVisitor extends CreateRecord
{
    protected static string $resource = VisitorResource::class;
}
