<?php

namespace App\Http\Middleware;

use Closure;
use Filament\Facades\Filament;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class Pengunjung
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if (Filament::auth()->check() && Filament::auth()->user()->role=='pengunjung') {
            return $next($request);
        }
        if(Filament::auth()->user()->role=='pengunjung'){
            return redirect('/pengelola');
        }
        return redirect('/'.Filament::auth()->user()->role);
    }
}
