<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index($city){
        return view('product',['product'=>Product::where('city_id',$city)->get()]);
    }
    public function show(Product $id){
        dd($id);
    }
}
