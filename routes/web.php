<?php
use App\Http\Controllers\RegisterController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});
Route::get('register', [RegisterController::class, 'register'])->name('register');
Route::post('register/action', [RegisterController::class, 'actionregister'])->name('actionregister');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
//create route for product per city
Route::get('product/city/{city}', [App\Http\Controllers\ProductController::class, 'index'])->name('product');
Route::get('product/{id}', [App\Http\Controllers\ProductController::class, 'show'])->name('showproduct');
