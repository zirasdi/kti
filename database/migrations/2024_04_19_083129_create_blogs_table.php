<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->id();
            $table->text('content');
            $table->string('title'); #judul blog
            $table->foreignId('user_id')->constrained('users')->cascadeOnDelete(); #penulis blog
            $table->foreignId('product_id')->constrained('products')->cascadeOnDelete();
            $table->integer('visitor')->default(0);
            $table->string('image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('blogs');
    }
};
