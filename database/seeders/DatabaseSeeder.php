<?php

namespace Database\Seeders;

use App\Models\Blog;
use App\Models\Category;
use App\Models\City;
use App\Models\Comment;
use App\Models\Product;
use App\Models\User;
use App\Models\Visitor;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        User::factory(10)->create();


        // City::factory(10)->create();
        // Category::factory(10)->create();
        // User::factory(10)->create();
        // Product::factory(10)->create();
        // Blog::factory(10)->create();
        // Comment::factory(10)->create();
        // Visitor::factory(10)->create();
    }
}
